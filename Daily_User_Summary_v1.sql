select
                    daily_user.account_id accountid,
                                daily_user.user_id,
                                daily_user.user_name,
                                usergoals.productive_hrs_target_value user_productivity_goal,
                                usergoals.focused_hrs_target_value user_focus_goal,
                                daily_user.activity_date local_date,
                                CASE
                        when EXTRACT(DAYOFWEEK from daily_user.activity_date) = 3 then 'Tuesday'
                        when EXTRACT(DAYOFWEEK from daily_user.activity_date) = 4 then 'Wednesday'
                        when EXTRACT(DAYOFWEEK from daily_user.activity_date) = 2 then 'Monday'
                        when EXTRACT(DAYOFWEEK from daily_user.activity_date) = 5 then 'Thursday'
                        when EXTRACT(DAYOFWEEK from daily_user.activity_date) = 6 then 'Friday'
                        when EXTRACT(DAYOFWEEK from daily_user.activity_date) = 7 then 'Saturday'
                        when EXTRACT(DAYOFWEEK from daily_user.activity_date) = 1 then 'Sunday'
                    END AS day_of_week,
                                CASE
                        WHEN EXTRACT(DAYOFWEEK FROM daily_user.activity_date) in(1, 7) THEN 'Weekend'
                        ELSE 'Weekday'
                    END AS day_type,
                                DATE_TRUNC(daily_user.activity_date, WEEK) activity_week,
                                productive_activity_count,
                                unproductive_activity_count,
                                undefined_activity_count,
                                total_activity_count,
                                productive_active_duration_seconds,
                                productive_passive_duration_seconds,
                                unproductive_active_duration_seconds,
                                unproductive_passive_duration_seconds,
                                undefined_active_duration_seconds,
                                undefined_passive_duration_seconds,
                                active_duration_seconds,
                                total_duration_seconds,
                                productive_active_duration_seconds + productive_passive_duration_seconds - collaboration_duration_seconds - context_switch_duration_seconds focused_duration_seconds,
                                collaboration_activity_count,
                                collaboration_duration_seconds,
                                context_switch_count attention_shift_activity_count,
                                context_switch_duration_seconds multitasking_duration_seconds,
                                break_count,
                                break_duration_seconds,
                                unproductive_activity_count + undefined_activity_count as non_business_activity_count,
                                unproductive_active_duration_seconds
                        + undefined_active_duration_seconds
                        + unproductive_passive_duration_seconds
                        + undefined_passive_duration_seconds as non_business_activity_duration_seconds,
                                productive_session_count,
                                focused_session_count,
                                productive_session_duration_seconds,
                                focused_session_duration_seconds,
                                first_activity_datetime,
                                last_activity_datetime,
                                first_activity_minutes_in_day,
                                last_activity_minutes_in_day,
                                CASE WHEN ((productive_active_duration_seconds + productive_passive_duration_seconds)/3600) > (COALESCE(usergoals.productive_hrs_target_value, 6.0) * 1.3) THEN 'Overutilized'
                                    WHEN ((productive_active_duration_seconds + productive_passive_duration_seconds)/3600) < (COALESCE(usergoals.productive_hrs_target_value, 6.0) * 0.7) THEN 'Underutilized'
                                    ELSE 'Healthy'
                                END AS utilization_level
                from `com-activtrak-us-ai-prod.activinsights_daily_user_summary.604700` as daily_user
                left join `com-activtrak-us-ai-prod.activinsights_user.604700` as ai_users on ai_users.user_id = daily_user.user_id                
                left join(
                        select
                            group_id,
                            any_value(case when metric_name = 'productive_hrs_day' then target_value else null end) as productive_hrs_target_value, 
                            any_value(case when metric_name = 'focused_hrs_day' then target_value else null end) as focused_hrs_target_value
                        from (
                                SELECT     
                                    targets.group_id, 
                                    metric.metric_name,         
                                    targets.target_value_last_refresh as target_value
                                FROM `com-activtrak-us-ai-prod.activinsights_user_group_targets.604700` targets
                                    join `com-activtrak-us-ai-prod.activinsights.metric` metric on targets.metric_id=metric.metric_id
                                    left join `com-activtrak-us-ai-prod.activinsights.benchmark` benchmark on targets.benchmark_id=benchmark.benchmark_id
                            )    
                        group by group_id
                ) as usergoals on ai_users.primary_group_id = usergoals.group_id