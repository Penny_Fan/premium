select 
	                 activities.account_id accountid
	                ,activities.user_id
	                ,activities.user_name
	                ,activities.activity_date AS local_date
	                ,CASE WHEN EXTRACT(DAYOFWEEK FROM activities.activity_date) IN (1,7) THEN 'Weekend' ELSE 'Weekday' END AS day_type
	                ,CASE
                      WHEN EXTRACT(DAYOFWEEK from activities.activity_date) = 2 then 'Monday'
                      WHEN EXTRACT(DAYOFWEEK from activities.activity_date) = 3 then 'Tuesday'
                      WHEN EXTRACT(DAYOFWEEK from activities.activity_date) = 4 then 'Wednesday'
                      WHEN EXTRACT(DAYOFWEEK from activities.activity_date) = 5 then 'Thursday'
                      WHEN EXTRACT(DAYOFWEEK from activities.activity_date) = 6 then 'Friday'
                      WHEN EXTRACT(DAYOFWEEK from activities.activity_date) = 7 then 'Saturday'
                      WHEN EXTRACT(DAYOFWEEK from activities.activity_date) = 1 then 'Sunday'
                    END AS day_of_week
	                ,DATE_TRUNC(activities.activity_date, WEEK) activity_week
	                ,activities.computer_id
	                ,activities.passive_type_id
	                ,activities.executable
	                ,activities.description AS application
  	                ,CONCAT(COALESCE(activities.description,activities.executable),'(',activities.host,')') AS browser_site
	                ,CASE WHEN activities.host IS NULL THEN activities.application ELSE activities.host END AS application_or_site
  	                ,activities.host
  	                ,activities.website
  	                ,activities.activity_type
	                ,activities.category
	                ,activities.productivity
	                ,duration_seconds
                FROM `com-activtrak-us-ai-prod.activinsights_user.604700` user
                    LEFT JOIN `com-activtrak-us-ai-prod.activinsights_group.604700` user_group
                    ON user.account_id = user_group.account_id AND user.primary_group_id = user_group.group_id
                    LEFT JOIN `com-activtrak-us-ai-prod.activinsights_daily_activity_summary.604700` AS activities
                    ON activities.account_id = user.account_id AND activities.user_id = user.user_id